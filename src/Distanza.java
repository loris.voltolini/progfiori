
public class Distanza implements Comparable<Distanza> {
    //attributi
    private double distanza;
    private String tipo;

    //costruttori
    public Distanza(){}
    public Distanza(double distanza, String tipo)
        {
            this.distanza=distanza;
            this.tipo=tipo;
        }
    public Distanza(Distanza d)
        {
            this.distanza=d.getDistanza();
            this.tipo=d.getTipo();
        }

    public double getDistanza() {return this.distanza;}
    public void setDistanza(double distanza) {this.distanza = distanza;}
    public String getTipo() { return this.tipo;}
    public void setTipo(String tipo) { this.tipo = tipo;}

     //metodo equals
    @Override
    public boolean equals(Object obj)
         {
             if(obj instanceof Distanza)
                 {
                     return equals(obj);
                 }
             else
                 {
                     return false;
                 }
         }
 
    public boolean equals(Distanza d)
        {
             return this.distanza==d.getDistanza() && this.tipo.equals(d.getTipo());
        }
  
    public int compareTo(Distanza d)
        {
                int result=0;
                if(this.getDistanza()<d.getDistanza())
                    {
                        result=-1;
                    }
                else
                    {
                        if(this.getDistanza()==d.getDistanza())
                            {
                                result=0;
                            }
                        else
                            {
                                if(this.getDistanza()>d.getDistanza())
                                    {
                                        result=1;
                                    }
                            } 
                    }
            return result;
        }
    //toString

    @Override
    public String toString() {
        return " distanza='" + getDistanza() + "'" +
            ", tipo='" + getTipo() + "'" + System.lineSeparator();
    }


   }
