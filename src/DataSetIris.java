import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;



public class DataSetIris {
    //attributi
    private ArrayList <Fiore> dataSet = new ArrayList<>();

    //costruttore
    public DataSetIris()
        {
            dataSet= new ArrayList<Fiore>();
        }
     //metodo per inserire un fiore
     public void InserisciFiore(Fiore f)
     {
         dataSet.add(f);
     }
    //metodo load Csv
    public void loadCSV(String nomeFile) throws IOException
        {
            Scanner scanner= new Scanner(new File(nomeFile));
            try {
                while(scanner.hasNext())
                {
                    String []valori = scanner.next().split(",");
                    dataSet.add(new Fiore(Double.valueOf(valori[0]), Double.valueOf(valori[1]), Double.valueOf(valori[2]), Double.valueOf(valori[3]), valori[4]));
                }
            } catch (Exception e) {
                //TODO: handle exception
            }
            finally
                {
                    scanner.close();
                }
        }
   //metodo KNN con K=1
   public String KNN1(double sepal_length, double sepal_width, double petal_length, double petal_width)
   {
       int k=1;
       ArrayList <Distanza> temp = new ArrayList<>();
       String KNN="";
       for(Fiore f: dataSet)
       {    
        //radice quadarata (x2-x1)^2+(y2-y1)^2
        double distanza= Math.sqrt(Math.pow(sepal_length- f.getSepal_length(),2)+Math.pow(sepal_width- f.getSepal_width(),2)+ Math.pow(petal_length - f.getPetal_length(),2)+Math.pow(petal_width - f.getPetal_width(),2));
         temp.add(new Distanza(distanza,f.getIris_class()));
       
        }
        Collections.sort(temp);
        KNN="iris class= " + temp.get(0).getTipo();
        return KNN;
   }
    //metodo KNN 
    public String KNN(double sepal_length, double sepal_width, double petal_length, double petal_width, int k)
    {
       ArrayList <Distanza> temp = new ArrayList<>();
       String KNN="";
       int contaS=0;;
       int contaVe=0;
       int contaVi=0;
       String s;
       for(Fiore f: dataSet)
        {    
            //radice quadarata (x2-x1)^2+(y2-y1)^2
            //distanze[i]=Math.sqrt(Math.pow(sepal_length, f.getSepal_length())+Math.pow(sepal_width, f.getSepal_width())+ Math.pow(petal_length, f.getPetal_length())+Math.pow(petal_width, f.getPetal_width()));
            double distanza= Math.sqrt(Math.pow(sepal_length- f.getSepal_length(),2)+Math.pow(sepal_width- f.getSepal_width(),2)+ Math.pow(petal_length - f.getPetal_length(),2)+Math.pow(petal_width - f.getPetal_width(),2));
            temp.add(new Distanza(distanza,f.getIris_class()));
        
         }
        Collections.sort(temp);
        for(int i=0; i< k; i++)
            {
                
                if(temp.get(i).getTipo().equals("Iris-setosa"))
                    {
                        contaS+=1;
                    }
                else
                {
                    if(temp.get(i).getTipo().equals("Iris-versicolor"))
                        {
                            contaVe+=1;
                        }
                    else
                    {
                        if(temp.get(i).getTipo().equals("Iris-virginica"))
                            {
                                contaVi+=1;
                            }
                    }
                }
                
            }
        if(contaS<contaVe)
            {
                if(contaVe<contaVi)
                    {
                        KNN= "Iris-virginica";
                        
                    }
                else
                    {
                        KNN= "Iris-versicolor";
                    }
            }
        else
                {
                    if(contaS>contaVi)
                    {
                        KNN="Iris-setosa";
                    }
                    else
                    {
                        KNN="Iris-virginica";
                    }
                }
       
        double percentualeS= contaS*100.0/k;
        String perc1 =String.valueOf(percentualeS);
        double percentualeVe= contaVe*100.0/k;
        String perc2 =String.valueOf(percentualeVe);
        double percentualeVi= contaVi*100.0/k;
        String perc3 =String.valueOf(percentualeVi);
        s="percentuale Iris-setosa:  " +perc1 +"%" +System.lineSeparator()+
        "percentuale Iris-versicolor:  " +perc2 +"%" +System.lineSeparator()+
        "percentuale Iris-virginica:  " +perc3 +"%" +System.lineSeparator()+
        "fiore predetto :" +KNN+System.lineSeparator();
        return s;
    }
   //metodo toString ()
   @Override
   public String toString()
   {
       String s="";
       for (Fiore f: dataSet)
       {
           s+=f.toString();
       } 
       return s;
   }
    
}
