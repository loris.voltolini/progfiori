public class Fiore {
    //attributi
    private double sepal_length;
    private double sepal_width;
    private double petal_length;
    private double petal_width;
    private String iris_class;

    //costruttore
    public Fiore(){}
    public Fiore(double at1, double at2, double at3, double at4, String at5)
        {
            this.sepal_length=at1;
            this.sepal_width=at2;
            this.petal_length=at3;
            this.petal_width=at4;
            this.iris_class=at5;

        }
    
    public Fiore(Fiore f)
    {
        this.sepal_length=f.getSepal_length();
        this.sepal_width=f.getSepal_width();
        this.petal_length=f.getPetal_length();
        this.petal_width=f.getPetal_width();
        this.iris_class=f.getIris_class();
    }

    public double getSepal_length() { return this.sepal_length;}
    public void setSepal_length(double sepal_length) {   this.sepal_length = sepal_length;}
    public double getSepal_width() {   return this.sepal_width;}
    public void setSepal_width(double sepal_width) { this.sepal_width = sepal_width;}
    public double getPetal_length() {    return this.petal_length;}
    public void setPetal_length(double petal_length) {    this.petal_length = petal_length;}
    public double getPetal_width() {    return this.petal_width;}
    public void setPetal_width(double petal_width) {    this.petal_width = petal_width;}
    public String getIris_class() {    return this.iris_class;}
    public void setIris_class(String iris_class) {   this.iris_class = iris_class; }
    //metodo equals
    @Override
    public boolean equals(Object obj)
        {
            if(obj instanceof Fiore)
                {
                    return equals(obj);
                }
            else
                {
                    return false;
                }
        }

    public boolean equals(Fiore f)
        {
            return this.sepal_length==f.getSepal_length() && this.sepal_width==f.getSepal_width() && this.petal_length==f.getPetal_length() && this.petal_width==f.getPetal_width() && this.iris_class.equals(f.getIris_class());
        }
    //metodo compare to

    //metodo ToString

    @Override
    public String toString() 
        {
            return " sepal_length='" + getSepal_length() + "'" +
                ", sepal_width='" + getSepal_width() + "'" +
                ", petal_length='" + getPetal_length() + "'" +
                ", petal_width='" + getPetal_width() + "'" +
                ", iris_class='" + getIris_class() + "'" + System.lineSeparator();
        }



    
}
